﻿using Grafika.HelpWindows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace Grafika.Helpers
{
    public static class Editor
    {
        public static void SaveShape(Shape shape, SquareOptions squareOpt, TypeOfShape type)
        {
         

            if (type == TypeOfShape.Rectangle)
            {
                ((Rectangle)shape).Width = squareOpt.SideA;
                ((Rectangle)shape).Height = squareOpt.SideB;
                Canvas.SetLeft(shape, squareOpt.PositionX);
                Canvas.SetTop(shape, squareOpt.PositionY);
            }

           else if ( type == TypeOfShape.Line)
            {
                ((Line)shape).X1 = squareOpt.P1_X;
                ((Line)shape).X2 = squareOpt.P2_X;
                ((Line)shape).Y1 = squareOpt.P1_Y;
                ((Line)shape).Y2 = squareOpt.P2_Y;
            }

           else if (type == TypeOfShape.Circle)
            {
                ((Ellipse)shape).Height = squareOpt.Height;
                ((Ellipse)shape).Width = squareOpt.Width;
                Canvas.SetLeft(shape, squareOpt.PositionX);
                Canvas.SetTop(shape, squareOpt.PositionY);
            }
        }

    }
}
