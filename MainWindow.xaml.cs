﻿using Grafika.Helpers;
using Grafika.HelpWindows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Grafika
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Shape> Shapes { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            Shapes = new List<Shape>();
        }


        private void DrawCircleClick(object sender, RoutedEventArgs e)
        {
            var squareOpt = new SquareOptions(TypeOfShape.Circle);
            squareOpt.ShowDialog();
            var rect = new Ellipse()
            {
                Stroke = new SolidColorBrush(Colors.Black),
                Fill = new SolidColorBrush(Colors.Black),
                StrokeThickness = 2
            };

            Editor.SaveShape(rect, squareOpt, TypeOfShape.Circle);

            EditCanvas.Children.Add(rect);
            rect.MouseDown += EditOpt_MouseDown;
            rect.Name += "Shape" + Shapes.Count;
            Shapes.Add(rect);
        }

        private void DrawLineClick(object sender, RoutedEventArgs e)
        {
            var squareOpt = new SquareOptions(TypeOfShape.Line);
            squareOpt.ShowDialog();
            var rect = new Line()
            {
                Stroke = new SolidColorBrush(Colors.Black),
                Fill = new SolidColorBrush(Colors.Black),
                StrokeThickness = 2
            };

            Editor.SaveShape(rect, squareOpt, TypeOfShape.Line);

            EditCanvas.Children.Add(rect);
            rect.MouseDown += EditOpt_MouseDown;
            rect.Name += "Shape" + Shapes.Count;
            Shapes.Add(rect);
        }

        private void DrawRectangleClick(object sender, RoutedEventArgs e)
        {
            var squareOpt = new SquareOptions(TypeOfShape.Rectangle);
            squareOpt.ShowDialog();
            var rect = new Rectangle()
            {
                Stroke = new SolidColorBrush(Colors.Black),
                Fill = new SolidColorBrush(Colors.Black),
            };

            Editor.SaveShape(rect, squareOpt, TypeOfShape.Rectangle);

            EditCanvas.Children.Add(rect);
            rect.MouseDown += EditOpt_MouseDown;
            rect.Name += "Shape" + Shapes.Count;
            Shapes.Add(rect);
        }

        private void EditOpt_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var shape = Shapes.FirstOrDefault(x => x.Name == ((Shape)sender).Name);

            TypeOfShape typeOfShape = TypeOfShape.Line;

            if (shape is Rectangle)
                typeOfShape = TypeOfShape.Rectangle;
            if (shape is Line)
                typeOfShape = TypeOfShape.Line;
            if (shape is Ellipse)
                typeOfShape = TypeOfShape.Circle;

            var squareOpt = new SquareOptions(shape, typeOfShape);
            squareOpt.ShowDialog();

            Editor.SaveShape(shape, squareOpt, typeOfShape);

            //((Rectangle)shape).Width = squareOpt.SideLength;
            //((Rectangle)shape).Height = squareOpt.SideLength;
            //Canvas.SetLeft(shape, squareOpt.PositionX);
            //Canvas.SetTop(shape, squareOpt.PositionY);

        }

    }
}
