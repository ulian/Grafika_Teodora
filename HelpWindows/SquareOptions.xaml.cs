﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Grafika.HelpWindows
{
    /// <summary>
    /// Interaction logic for SquareOptions.xaml
    /// </summary>
    public partial class SquareOptions : Window
    {
        //universal
        public int PositionX { get; set; }
        public int PositionY { get; set; }

        //rectangle
        public double SideA { get; set; }
        public double SideB { get; set; }
        // line
        public int P1_X { get; set; }
        public int P1_Y { get; set; }
        public int P2_X { get; set; }
        public int P2_Y { get; set; }

        //ellipse
        public int Height { get; set; }
        public int Width { get; set; }

        public TypeOfShape MyTypeOfShape { get; set; }

        //public SquareOptions()
        //{
        //    InitializeComponent();

        //}

        public SquareOptions(TypeOfShape typeOfShape)
        {
            InitializeComponent();
            MyTypeOfShape = typeOfShape;
            if(typeOfShape == TypeOfShape.Rectangle)
            {
                L1.Content = "Szerokość";
                L2.Content = "Wysokość";
                L3.Content = "Położenie X";
                L4.Content = "Położenie Y";
            }
            else if (typeOfShape == TypeOfShape.Line)
            {
                L1.Content = "P1 X";
                L2.Content = "P1 Y";
                L3.Content = "P2 X";
                L4.Content = "P2 Y";
            }
            else if (typeOfShape == TypeOfShape.Circle)
            {
                L1.Content = "Szerokość";
                L2.Content = "Wysokość";
                L3.Content = "Środek X";
                L4.Content = "Środek Y";
            }
        }

        public SquareOptions(Shape shape, TypeOfShape typeOfShape) : this(typeOfShape)
        {
            if (typeOfShape == TypeOfShape.Rectangle)
            {
                T1.Text = ((Rectangle) shape).ActualWidth.ToString();
                T2.Text = ((Rectangle) shape).ActualHeight.ToString();
                T3.Text = Canvas.GetLeft(shape).ToString();
                T4.Text = Canvas.GetTop(shape).ToString();
                
            }
            else if (typeOfShape == TypeOfShape.Line)
            {
                T1.Text = ((Line)shape).X1.ToString();
                T2.Text = ((Line)shape).Y1.ToString();
                T3.Text = ((Line)shape).X2.ToString();
                T4.Text = ((Line)shape).Y2.ToString();
            }

            else if (typeOfShape == TypeOfShape.Circle)
            {
                T1.Text = ((Ellipse)shape).Width.ToString();
                T2.Text = ((Ellipse)shape).Height.ToString();
                T3.Text = Canvas.GetLeft(shape).ToString();
                T4.Text = Canvas.GetTop(shape).ToString();
            }

            //SideLength = shape.ActualHeight;
            //SideLengthTextBox.Text = SideLength.ToString();
            //PositionYTextBox.Text =  Canvas.GetTop(shape).ToString();
            //PositionXTextBox.Text =  Canvas.GetLeft(shape).ToString();
        }

        private void SaveSquare(object sender, RoutedEventArgs e)
        {
            if (MyTypeOfShape == TypeOfShape.Rectangle)
            {
                SideA = int.Parse(T1.Text);
                SideB = int.Parse(T2.Text);
                PositionX = int.Parse(T3.Text);
                PositionY = int.Parse(T4.Text);
            }
           else if (MyTypeOfShape == TypeOfShape.Line)
            {
                P1_X = int.Parse(T1.Text);
                P1_Y = int.Parse(T2.Text);
                P2_X = int.Parse(T3.Text);
                P2_Y = int.Parse(T4.Text);
            }

            else if (MyTypeOfShape == TypeOfShape.Circle)
            {
                Width = int.Parse(T1.Text);
                Height = int.Parse(T2.Text);
                PositionX = int.Parse(T3.Text);
                PositionY = int.Parse(T4.Text);
            }
            this.Close();
        }
    }
}
