﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafika
{
    public enum TypeOfShape
    {
        Rectangle,
        Line,
        Circle
    }
}
